import { Photo } from './photo';

export interface User {
    id: number;
    userName: string;
    knownAs: string;
    age: number;
    gender: string;
    created: Date;
    lastActive: Date;
    photoUrl: string;
    city: string;
    country: string;
    // tieto properties budu ziskane z UserForListDto

    // ale tiez tu budu properties z UserForDetailedDto
    interests?: string;
    // ? volitelne - musia ist vzdy po povinnych, inak by to vyvolalo chybu
    introduction?: string;
    lookingFor?: string;
    photos?: Photo[];
    roles?: string[];
}
