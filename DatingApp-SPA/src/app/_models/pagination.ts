export interface Pagination {
    // tu replikujeme informácie ktoré dostaneme v pagination header(pozri 139 na konci s Postmanom)
    currentPage: number;
    itemsPerPage: number;
    totalItems: number;
    totalPages: number;
}

// je to typ T pretože to budeme používať univerzálne pre userov, messages...
export class PaginatedResult<T> {
    result: T; // tu budeme ukladať napr. userov
    pagination: Pagination; // tu sa uložia 4 premené čo sú navrchu a pochádzajú z hlavičky, ktorá bola vrátená
}
