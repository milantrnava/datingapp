import { Injectable } from '@angular/core';

// 6/53 pretože sme pridali Alertify do angular.json, je k dispozícii globálne v našej applikácii,
// preto Alertify nepotrebujeme importovať ale ho iba deklarujeme pre tento .ts
declare let alertify: any;

@Injectable({
  providedIn: 'root'
})
export class AlertifyService {

constructor() { }

// 6/53 okCallback je funkcia typu any, e reprezentuje klik event
confirm(message: string, okCallback: () => any) {
  alertify.confirm(message, function(e) {
    if (e) {
      okCallback();
    } else {}
  });
}

success(message: string) {
  alertify.success(message);
}

error(message: string) {
  alertify.error(message);
}

warning(message: string) {
  alertify.warning(message);
}

message(message: string) {
  alertify.message(message);
}
}
