import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  model: any = {}; // 4/38 potrebujeme vytvoriť objekt, do ktorého uložíme meno a heslo z formulára
  photoUrl: string;

  // 4/40 zmena authService na public, pretože ho používame v html a javascript nepozná private
  // a preto tam bola chyba
  constructor(public authService: AuthService, private alertify: AlertifyService,
    private router: Router) { }

  ngOnInit() {
    this.authService.currentPhotoUrl.subscribe(photoUrl => this.photoUrl = photoUrl);
  }

  login() {
    this.authService.login(this.model).subscribe(next => {
      this.alertify.success('Logged in successfully');
    }, error => {
      this.alertify.error(error);
    }, () => {
      this.router.navigate(['/members']);
    }
    );
  }

  loggedIn() {
    return this.authService.loggedIn();
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user'); // 11/114
    this.authService.decodedToken = null; // 11/114
    this.authService.currentUser = null; // 11/114
    this.alertify.message('Logged out');
    this.router.navigate(['/home']);
  }

}
