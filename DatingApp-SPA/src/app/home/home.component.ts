import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  registerMode = false; // 4/42 prepínač či sa zobrazí na stránke úvodná obrazovka alebo registrácia

  constructor(private http: HttpClient) { }

  ngOnInit() {}

  registerToggle() {
    this.registerMode = true; // 4/44
    // this.registerMode = !this.registerMode;
  }

  cancelRegisterMode(registerMode: boolean) {
    this.registerMode = registerMode; // príde false z RegisterComponent
  }
}
