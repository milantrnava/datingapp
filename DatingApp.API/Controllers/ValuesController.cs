﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.API.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Controllers
{
    [Route("api/[controller]")]  // controller je zástupný názov pre prvú časť názvu kontrolera, [controller] = values ...z ValuesController
    [ApiController]
    public class ValuesController : ControllerBase
    // tu by sa dalo dediť aj z Controller, ktorý má view support 
    // ale my toto používame iba ako API a tak použijeme ControllerBase, ktorý je bez view support.
    {
        private readonly DataContext _context; //2/10
        public ValuesController(DataContext context)
        {
            _context = context;
        }
        // GET api/values
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> GetValues()
        {
            //var values = _context.Values.ToList();
            var values = await _context.Values.ToListAsync();
            //príručky uvádzajú, že treba používať async tak často ako je to možné, zlepšuje sa tým aj výkon aplikácie
            return Ok(values);
        }

        // GET api/values/5
        //[AllowAnonymous] //3/34
        [Authorize(Roles = "Member")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetValue(int id) //2/10 toto id je z HttpGet, z adresy v prehliadači
        {
            var value = await _context.Values.FirstOrDefaultAsync(x => x.Id == id);
            //2/10 je lepšie použiť FirstOrDefault namiesto First, v prípade že zadané id neexistuje
            return Ok(value);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
