using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DatingApp.API.Data;
using DatingApp.API.Dtos;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace DatingApp.API.Controllers
{
    [AllowAnonymous] // 198
    [Route("api/[controller]")]
    [ApiController] //3/31 popis ako toto funguje
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config; //3/33
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AuthController(IConfiguration config, IMapper mapper,
            UserManager<User> userManager,
            SignInManager<User> signInManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _mapper = mapper;
            _config = config;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegisterDto userForRegisterDto) //3/29+30
        {
            var userToCreate = _mapper.Map<User>(userForRegisterDto); // 128 User je cieľ, userForRegister je zdroj

            var result = await _userManager.CreateAsync(userToCreate, userForRegisterDto.Password);

            var userToReturn = _mapper.Map<UserForDetailedDto>(userToCreate); // 128

            if (result.Succeeded)
            {
                return CreatedAtRoute("GetUser",
                    new { controller = "Users", id = userToCreate.Id}, userToReturn);
            }

            return BadRequest(result.Errors);
            // 128 GetUser - Name of the route (pozri UsersContr.), userToReturn - je čo chceme vrátiť späť,
            // nechceme použiť createdUser, pretože obsahuje heslo a ani nebudeme kvôli tomu vytvárať nový Dto ale
            // využijeme existujúci UserForDetailedDto
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDto userForLoginDto)
        {
            var user = await _userManager.FindByNameAsync(userForLoginDto.Username);

            var result = await _signInManager
                .CheckPasswordSignInAsync(user, userForLoginDto.Password, false);

            if (result.Succeeded)
            {
                var appUser = await _userManager.Users.Include(p => p.Photos)
                    .FirstOrDefaultAsync(u => u.NormalizedUserName == userForLoginDto.Username.ToUpper());

                var userToReturn = _mapper.Map<UserForListDto>(appUser);

            return Ok(new
            {
                token = GenerateJwtToken(appUser).Result,
                user = userToReturn // 11/114 doplnené kvôli main photo vpravo hore
            });
            }

            return Unauthorized();
        }
            // 11/114 vpravo hore chceme fotku mainPhoto užívateľa po prihlásení,
            // dal by sa spraviť kvôli tomu nový Dto ale postačí aj existujúci UserForListDto

            // vrátime token ako objekt klientovi (súbor .jwt)

        private async Task<string> GenerateJwtToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()), //3/33 Claim očakáva string preto je tam ToString
                new Claim(ClaimTypes.Name, user.UserName)
            };

            var roles = await _userManager.GetRolesAsync(user);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8
                .GetBytes(_config.GetSection("AppSettings:Token").Value));
            //3/33 v appsettings.json v sekcii AppSettings je uložený token, mal by mať minimálne 12 znakov,
            // najlepšie ale veľmi dlhý sled náhodne generovaných znakov. Ak niekto získa token, mohol by potom predstierať
            // že je nejaký užívateľ a robiť dopyty na server

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor //3/33
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1), //platnosť vyprší za 24h
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            // s handlerom môžeme vytvoriť token a dať ho do tokenDescriptor

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}