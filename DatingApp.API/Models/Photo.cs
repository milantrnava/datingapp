using System;

namespace DatingApp.API.Models
{
    public class Photo
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public bool IsMain { get; set; }
        public string PublicId { get; set; } // pridane kvôli Cloudinary

        public bool IsApproved { get; set; } // 216 ak nič neuvedieme, defaultne bude
        // nastavené nullable: false, defaultValue: false
        
        /* pridane tieto 2 riadky kvoli kaskadovemu mazaniu, ak vymazem uzivatela,
        vymazu sa aj fotky. EF by dal do migracie UserId aj bez toho
        aby sme ho tu uviedli ale bolo by nastavene ako nullable: true
        ak sa uvedie ako teraz, bude nastavene nullable: false
        a kaskadove mazanie. */
        public User User { get; set; }
        public int UserId { get; set; }
    }
}
