namespace DatingApp.API.Models
{
    public class Like
    {
        // keďže sú tu dve ID EF Core nebude vedieť čo je primárny kľúč,
        // vyrieši sa to v DataContext
        public int LikerId { get; set; } // lajkovač

        public int LikeeId { get; set; } // lajkovaný

        public User Liker { get; set; }

        public User Likee { get; set; }
    }
}