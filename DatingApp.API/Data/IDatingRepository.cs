using System.Collections.Generic;
using System.Threading.Tasks;
using DatingApp.API.Helpers;
using DatingApp.API.Models;

namespace DatingApp.API.Data
{
    public interface IDatingRepository
    {
        void Add<T>(T entity) where T: class;
        /* 8/72 genericky typ metody, ...pridaj typ T (kde T je typ user, photo...)
        zober T ako entity, kde T je trieda. Miesto toho aby sme vytvarali Add metodu
        pre kazdu tabulku, tymto sposobom to bude univerzalne pouzitelne  */
        void Delete<T>(T entity) where T: class;
        Task<bool> SaveAll();
        // Metoda sa pozrie, ci su nejake zmeny, ak ano vrati TRUE
        Task<PagedList<User>> GetUsers(UserParams userParams); // 139 zmenené kvôli stránkovaniu Pagination
        // Task<IEnumerable<User>> GetUsers();
        Task<User> GetUser(int id, bool isCurrentUser);
        Task<Photo> GetPhoto(int id);
        Task<Photo> GetMainPhotoForUser(int userId);
        Task<Like> GetLike(int userId, int recipientId); // kto lajkol koho - overenie či lajk existuje
        Task<Message> GetMessage(int id); // vytiahnutie jednej správy z DB
        Task<PagedList<Message>> GetMessagesForUser(MessageParams messageParams);
        Task<IEnumerable<Message>> GetMessageThread(int userId, int recipientId);
        // komunikácia medzi dvoma užívateľmi
    }
}