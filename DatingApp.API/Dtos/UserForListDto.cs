using System;

namespace DatingApp.API.Dtos
{
    public class UserForListDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Gender { get; set; }
        public int Age {get; set; }
        // public DateTime DateOfBirth { get; set; }
        // namiesto datumu narodenia dame radsej iba vek
        public string KnownAs { get; set; }
        public DateTime Created { get; set; }
        // kedy bol vytvoreny profil
        public DateTime LastActive { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PhotoUrl { get; set; }
    }
}