﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DatingApp.API.Data;
using DatingApp.API.Helpers;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace DatingApp.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(x => x.UseSqlite (Configuration.GetConnectionString("DefaultConnection")));
            //2/8 Sqlite bolo potrebné doinštalovať cez nuget 
            //berie nastavenie z appsettings.json

            // 198
            IdentityBuilder builder = services.AddIdentityCore<User>(opt =>
            {
                opt.Password.RequireDigit = false;
                opt.Password.RequiredLength = 4;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = false;
            });

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);
            builder.AddEntityFrameworkStores<DataContext>();
            builder.AddRoleValidator<RoleValidator<Role>>();
            builder.AddRoleManager<RoleManager<Role>>();
            builder.AddSignInManager<SignInManager<User>>();

            //3/34 tu je špecifikovaná autentifikačná schéma
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII
                            .GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                        ValidateIssuer = false,  //3/34 false lebo pracujeme s localhost ???
                        ValidateAudience = false    //3/34 false lebo pracujeme s localhost ???
                    };
                });
            
            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireAdminRole", policy => policy.RequireRole("Admin"));
                options.AddPolicy("ModeratePhotoRole", policy => policy.RequireRole("Admin", "Moderator"));
                options.AddPolicy("VipOnly", policy => policy.RequireRole("VIP"));
            });
            
            // 198 doplnením options môžem vymazať všetky atribúty [Authorize] napr. v UserController
            services.AddMvc(options =>
                {
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    options.Filters.Add(new AuthorizeFilter(policy));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(opt => {
                    opt.SerializerSettings.ReferenceLoopHandling =
                    Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });
            // 8/73 vlozene kvoli chybe v api pri requeste na uzivatelov kvoli referencii na fotky - teraz bude chyba ignorovana

            services.AddCors();  //2/16 cross-origin resource sharing - aby mohol angular používať API
            services.Configure<CloudinarySettings>(Configuration.GetSection("CloudinarySettings"));
            // 11/103 tu spojime triedu CloudinarySettings v priečinku Helpers s nastavením
            // v appsettings.json
            Mapper.Reset(); // 198
            services.AddAutoMapper();
            services.AddTransient<Seed>();  // 8/71
            
            // services.AddScoped<IAuthRepository, AuthRepository>(); //3/28 -vytvorí inštanciu pre každý request v danom scoupe (nechápem)
            // services.AddSingleton - druhá možnosť -vytvorí single inštanciu repository a používa ju stále (je problém pri súbežných requestoch)
            // services.AddTransient - tretia možnosť -vytvorí inštanciu pri každom requeste
            services.AddScoped<IDatingRepository, DatingRepository>();
            
            services.AddScoped<LogUserActivity>(); // 134
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, Seed seeder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // app.UseHsts();   -zakomentované v sekcii 2, lekcia 5
                app.UseExceptionHandler(builder => {
                    builder.Run(async context => {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if (error != null)
                        {
                            context.Response.AddApplicationError(error.Error.Message);
                            await context.Response.WriteAsync(error.Error.Message);
                        }
                    });
                });  // 5/49
            }

            // app.UseHttpsRedirection();  //2/5
            seeder.SeedUsers();  // ak bude treba naplnit DB staci odkomentovat
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseAuthentication(); //3/34
            app.UseDefaultFiles(); // 175
            app.UseStaticFiles(); // 175 pri publikovaní
            // app.UseMvc();  // toto je middleware
            app.UseMvc(routes => {
                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Fallback", action = "Index"}
                );
            });
        }
    }
}