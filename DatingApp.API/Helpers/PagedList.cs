using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Helpers
{
    public class PagedList<T> : List<T>
    // 137 trieda bude univerzálne použiteľná na výpis Užívateľov, Správ...
    {
        // tieto informácie vrátime klientovi: CurrentPage, TotalPages...
        public int CurrentPage { get; set; } // tieto prop. budú pridané do Header v response (PaginationHeader)
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public PagedList(List<T> items, int count, int pageNumber, int pageSize)
        {
            TotalCount = count;
            PageSize = pageSize;
            CurrentPage = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            this.AddRange(items);
        }

        public static async Task<PagedList<T>> CreateAsync(IQueryable<T> source,
            int pageNumber, int pageSize)
        // táto metóda prinesie novú inštanciu triedy PagedList
        {
            var count = await source.CountAsync();
            var items = await source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
            // na každej strane chceme 5 položiek, príklad:
            // strana 3:   (3-1)*5=10 takže preskočí (skip) prvých 10 položiek a
            // zoberie (take) nasledujúcich 5 položiek (11,12,13,14,15) do listu 
            return new PagedList<T>(items, count, pageNumber, pageSize);
            // nevrátime len list s položkami ale aj count, pageNumber, pageSize
        }
    }
}