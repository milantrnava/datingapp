namespace DatingApp.API.Helpers
{
    public class UserParams
    {
        private const int MaxPageSize = 50; // prevencia aby niekto nemohol urobiť request na veľké množstvo Users
        public int PageNumber { get; set; } = 1; // pokiaľ nebude požadované inak, bude vždy vrátená 1
        private int pageSize = 10; // nastavenie počiatočnej hodnoty ak klient nestanoví túto hodnotu
        public int PageSize
        {
            get { return pageSize;}
            set { pageSize = (value > MaxPageSize) ? MaxPageSize : value;}
            // ak príde požiadavka (value) na viac ako MaxPageSize, nastaví sa MaxPageSize
        }
        public int UserId { get; set; }
        public string Gender { get; set; }
        public int MinAge { get; set; } = 18;
        public int MaxAge { get; set; } = 99;
        public string OrderBy { get; set; }
        public bool Likees { get; set; } = false;
        public bool Likers { get; set; } = false;
    }
}